# Spring Boot Microservices
Fontes do Curso Spring Boot Microservices da DevDojo feitos por mim.
# Arquitetura
 - Client Side
 - Router and Filter Gateway
 - Service Discovery Server ( Eureka Spring Cloud NetFlix )
 - DMZ
 - Authentication Service
 - Services

# Architecture
![](images/Architecture.png)

# Service Discovery Server
![](images/ServiceDiscovery.png)

# Security
![](images/Security.png)

# Tools
- Java 11.0.4
- Spring Boot 2.1.8

# Dependencies
 - DevTools
 - Lombok ( O Lombok é uma biblioteca Java focada em produtividade e redução de código boilerplate que por meio de 			anotações adicionadas ao nosso código ensinamos o compilador (maven ou gradle) durante o processo de 			compilação a criar código Java. )
 - Configuration Processor
 - Web
 - JPA
 - MySQL
 - H2

# Aulas 
  - Aula 03 : 
    - Criação do Projeto
	- Criação do Arquivo stack.yml na raiz do projeto
	- Renomear o arquivo application.properties para application.yml ( é de extrema importância que se escreva tudo respeitando      corretamente as quebras de linha e espaços pois pode dar problema e vc vai apanhar pra descobrir o que é )
	- docker-compose -f stack.yml up
	- Criar a conexão com database no docker 
	- Depois de Criar a conexão acessar o Mysql e criar o Banco de Dados ( Create Database devdojo; )
	
	- Criar src > main > java > academy.devdojo.youtube.course > endpoint > controller
	- Criar a classe CourseController.java
	
	- Criar src > main > java > academy.devdojo.youtube.course > endpoint > service
	- Criar a classe CourseService.java
	
	- Criar src > main > java > academy.devdojo.youtube.course > repository
	- Criar uma interface CourseRepository
	
	- Criar src > main > java > academy.devdojo.youtube.course > model
	- Criar uma interface AbstractEntity
    - Testar a API

  - Aula 04 : 
	- Modularizando o Projeto
	- Criar um novo arquivo pom.xml para o projeto o qual os módulos irão herdar dele as dependências.
	- Transformar a pasta course em um módulo do projeto
	- Criar o Módulo Core
	
